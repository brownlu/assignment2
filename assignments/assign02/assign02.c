#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "hardware/clocks.h"
#include "hardware/watchdog.h"
#include "hardware/pio.h"
#include "ws2812.pio.h"

#define WS2812_PIN 28 // The GPIO pin that the WS2812 connected to
#define NUM_PIXELS 1  // There is 1 WS2812 device in the chain
#define RGBW_STATE true  // Will use RGBW format

#define INITIAL_MSG "*************************WELCOME TO OUR MORSE CODE GAME*********************************\n*									           *\n*		LEARN MORSE CODE - An interactive game on the RP2040               *\n*									           *\n************************************************************************************\n*										   *\n*	Welcome to our game! This code was written by Group 27 in CSU23021	   *\n*										   *\n*			Some instructions to get you started:			   *\n*										   *\n*1. Choose your level by entering the morse code for the level number (don't worry *\n*   yet, these are provided for now.                                               *\n*2. Enter a (dot) by pressing the GP21 button on the MAKER-PI-PICO board for a     *\n*   short duration (255ms or less), and enter a (dash) by pressing the GP21        *\n*   button on the MAKER-PI-PICO board for a longer duration (longer than 255ms)    *\n*3. You will begin with 3 lives. Each time a correct answer is input, you will gain*\n*   a life (with 3 lives maximum), and each time your answer is incorrect, a life  *\n*   will be deducted. The RGB LED will indicate how many lives you have got left:  *\n*   				PURPLE: 3 lives                                    *\n*   			        ORANGE: 2 lives                                    *\n*                               RED: 1 life                                        *\n*                               OFF: 0 lives, GAME OVER                            *\n*										   *\n************************************************************************************\n" // welcome msg

// Must declare the main assembly entry point before use.
void main_asm();


/*-------------------------------------------- GPIO Initialzation -------------------------------------------- */

void asm_gpio_init(uint pin)
{
    gpio_init(pin);//number of pin being initialised
}

void asm_gpio_set_dir(uint pin, bool out)
{
    gpio_set_dir(pin, out);//I/O direction of pin being initialised
}

bool asm_gpio_get(uint pin)
{
    return gpio_get(pin);//getter function of a pin
}

void asm_gpio_put(uint pin, bool value)
{
    gpio_put(pin, value);//setter function of a pin
}

void asm_gpio_set_irq(uint pin)//enable rising/falling edges for IRQ
{
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, true);
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE, true);
}

/* -------------------------------------------- Watchdog initialization -------------------------------------------- */

void watchdog_init()
{
    if (watchdog_caused_reboot())
    { // check if reboot was due to watchdog timeout
        printf("\nNo input was detected for timeout period. Rebooted by watchdog\n");
    }
    if (watchdog_enable_caused_reboot())
    { // check if enabling watchdog caused reboot
        printf("\nChip reboot due to watchdog enable\n");
    }
    watchdog_enable(0x2328, 1); 
    watchdog_update();
}

static inline void put_pixel(uint32_t pixel_grb)
{
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b)
{
    return ((uint32_t)(r) << 8) |
           ((uint32_t)(g) << 16) |
           (uint32_t)(b);
}

/* -------------------------------------------- RGB LED Function -------------------------------------------- */

void RGB_Lives_Display(int lives)
{
    if (lives == 0)//player ran out of lives
    {
        put_pixel(urgb_u32(0x00, 0x00, 0x00)); // set led off 
        printf("Game   Over\n");
    }
    else if (lives == 1)//one life remaining
    {
        put_pixel(urgb_u32(0x7F, 0x00, 0x00)); // set LED color to red
        printf("Only 1 Lives remaining!.\n");
    }
    else if ( lives == 2)//2 lives remaining
    {
        put_pixel(urgb_u32(0xFF, 0xA5, 0x00)); // Set the  LED color to orange
        printf("2 Lives remaining.\n");
    }
    else if ( lives == 3)//full health
    {
        put_pixel(urgb_u32(0x4B, 0x00, 0x82)); // Set the  LED color to purple
        printf("3 Lives remaining.\n");
    }
    else
    {
        printf("Throw error detection.\n");
    }
}


/* -------------------------------------------- Global Variables -------------------------------------------- */

int Lives = 3; // Total lives the player has
int Extra_Lives = 0;    // Records the number of lives the player has gained
int Number_Correct = 0; // records the number of correct answers in a row
int Level_Index = 0;   //What life the player is on
char *userInput;     // This array stores the user inputs

char characters_entered[20];
int i = 0; // i = length of input sequence
int tmpIndex = 0;
int input_complete = 0;
int Remain_Counter = 5; // Maybe delete
int Wrong_Counter = 0; // Maybe delete as well

int input_state = 0;

/* -------------------------------------------- Morse Code Table -------------------------------------------- */
void MainIntro() {
    const char *pattern =
    "\n"
    "USE GP21 TO ENTER A SEQUENCE TO BEGIN\n"
    "\"- ----\" - LEVEL 01 - CHARS (EASY)\n"    //Level selection GUI display
    "\"- - ---\" - LEVEL 02 - CHARS (HARD)\n"
    "\".- ---\" - LEVEL 03 - WORDS (EASY)\n"
    "\".. - --\" - LEVEL 04 - WORDS (HARD)\n";
    
    printf("%s", pattern);
}
#define table_SIZE 36 //26 alphabetical chars + 10 numeric chars
typedef struct morsecode
{
    char character;
    char *code;
} morsecode;
morsecode table[table_SIZE];

void morse_init()
{
    table[0].character = 'A';table[1].character = 'B';table[2].character = 'C';table[3].character = 'D';table[4].character = 'E';table[5].character = 'F';
    table[6].character = 'G';table[7].character = 'H';table[8].character = 'I';table[9].character = 'J';table[10].character = 'K';
    table[11].character = 'L';table[12].character = 'M';table[13].character = 'N';table[14].character = 'O';table[15].character = 'P';
    table[16].character = 'Q';table[17].character = 'R';table[18].character = 'S';table[19].character = 'T';table[20].character = 'U';
    table[21].character = 'V';table[22].character = 'W';table[23].character = 'X';table[24].character = 'Y';table[25].character = 'Z';
    table[26].character = '0'; table[27].character = '1'; table[28].character = '2';table[29].character = '3';
    table[30].character = '4';table[31].character = '5';table[32].character = '6';table[33].character = '7';table[34].character = '8';table[35].character = '9';

    table[0].code = ".-";table[1].code = "-...";table[2].code = "-.-.";table[3].code = "-..";table[4].code = ".";table[5].code = "..-.";table[6].code = "--.";
    table[7].code = "....";table[8].code = "..";table[9].code = ".---";table[10].code = "-.-";table[11].code = ".-..";table[12].code = "--";table[13].code = "-.";
    table[14].code = "---";table[15].code = ".--.";table[16].code = "--.-";table[17].code = ".-.";table[18].code = "...";table[19].code = "-";table[20].code = "..-";
    table[21].code = "...-";table[22].code = ".--";table[23].code = "-..-";table[24].code = "-.--";table[25].code = "--..";table[26].code = "-----";table[27].code = ".----";
    table[28].code = "..---";table[29].code = "...--";table[30].code = "....-";table[31].code = ".....";table[32].code = "-....";
    table[33].code = "--...";table[34].code = "---..";table[35].code = "----.";
}

static const char *morseCodeMapping[] = {
    ".-",    // A
    "-...",  // B
    "-.-.",  // C
    "-..",   // D
    ".",     // E
    "..-.",  // F
    "--.",   // G
    "....",  // H
    "..",    // I
    ".---",  // J
    "-.-",   // K
    ".-..",  // L
    "--",    // M
    "-.",    // N
    "---",   // O
    ".--.",  // P
    "--.-",  // Q
    ".-.",   // R
    "...",   // S
    "-",     // T
    "..-",   // U
    "...-",  // V
    ".--",   // W
    "-..-",  // X
    "-.--",  // Y
    "--..",  // Z
    "-----", // 0
    ".----", // 1
    "..---", // 2
    "...--", // 3
    "....-", // 4
    ".....", // 5
    "-....", // 6
    "--...", // 7
    "---..", // 8
    "----.", // 9
};

static const char *alphaNumericList[] = {
"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",
};
/* -------------------------------------------- Button pressed Timing -------------------------------------------- */

int current_time()
{
    absolute_time_t time = get_absolute_time(); //system time 
    return to_ms_since_boot(time);
}

int time_difference(int end_time, int start_time)
{
    return (end_time - start_time);     //calculate time taken
}

/* -------------------------------------------- Starting Game -------------------------------------------- */

void startGame()
{
    put_pixel(urgb_u32(0x00, 0x7F, 0x00)); // Set the RGB LED color to green
    Number_Correct= 0;                   //Correct answers = 0
    Lives = 3;                           //Lives = full health
    Extra_Lives = 0;                     //Bonus lives = N/A                  
}  
/* -------------------------------------------- Level Selection -------------------------------------------- */

void level_select_true(){ //Function called from the ASM code
    input_state = 1;
}

void level_select_false(){//Function called from the ASM code
    input_state = 0;
}

int select_level(){

    if (strcmp(characters_entered, morseCodeMapping[27])==0){    // return 1 for level 1
        printf("Level 1 selected");
        return 1;
    }
    else if(strcmp(characters_entered, morseCodeMapping[28])==0){    // return 2 for level 2
        printf("Level 2 selected");
        return 2;
    }
}
// Random number generator, for getting a random letter/number from the morese code table
int random_number(int low, int high)
{
    if (low > high)
        return high;
    return low + (rand() % (high - low + 1));
}
int level_1_question()
{
    Level_Index = 1;
    int tmp = random_number(0, 35); //Randomly select from the list of 36 chars
    tmpIndex = tmp;
    printf("\n");
    printf("____________________________________________________\n");
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]     Translate this character to Morse code     []\n");
    printf("[]                       %s                       []\n",morseCodeMapping[tmp]);
    printf("[]                       %s                       []\n",alphaNumericList[tmp]);
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    return tmp; //Return the generated number's index from the table
}

int level_2_question()
{
    Level_Index = 2;
    int tmp = random_number(0, 35); //Randomly select from the list of 36 chars
    tmpIndex = tmp;
    printf("\n");
    printf("____________________________________________________\n");
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]           What is this in morse code?          []\n");
    printf("[]                       %c                       []\n",table[tmp].character);
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    return tmp; //Return the generated number's index from the table
}
int get_level()
{
    return Level_Index; //getter function for the level
}

void set_level(int level)
{
    Level_Index = level;//setter function for the level
}
/*-------HANDLING END OF LEVEL----------*/
int check_level_complete()
{
    if (Number_Correct == 5)    //5 correct answers means level complete
    {
        return 1;
    }
    if (Lives == 0)            //0 lives means the game is lost
    {
        printf("\n");
        printf("____________________________________________________\n");
        printf("[]________________________________________________[]\n");
        printf("[]                                                []\n");
        printf("[]                                                []\n");
        printf("[]               GAME OVER; You lose              []\n");
        printf("[]           You have no lives remaining          []\n");
        printf("[]                                                []\n");
        printf("[]________________________________________________[]\n");
        printf("[]________________________________________________[]\n");
        //Total statistics for entire gmae
        printf("\n");
        printf("____________________________________________________\n");
        printf("[]________________________________________________[]\n");
        printf("[]               ~ ~ GAME STATS ~ ~               []\n");
        printf("[]                                                []\n");
        printf("[]               Correct answers: %d              []\n", Number_Correct);
        printf("[]              Incorrect answers: %d             []\n", Wrong_Counter);
        printf("[]               Lives collected: %d              []\n", Extra_Lives);
        printf("[]               Remianing Lives: %d              []\n", Lives);
        printf("[]________________________________________________[]\n");
        printf("[]________________________________________________[]\n");
        printf("\n");
        return 2;
    }
return 0; //Return from game
}

/*-------HANDLING END OF GAME----------*/
void game_won_display()
{
    printf("____________________________________________________\n")
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]                Congratulations!                []\n");
    printf("[]              You've won the game!              []\n");
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
}

void game_over_display()
{
    printf("____________________________________________________\n")
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]                    Hard luck,                  []\n");
    printf("[]              You've lost the game.             []\n");
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
}

void level_selection_invalid(){
    printf("____________________________________________________\n")
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]            Your input was invalid,             []\n");
    printf("[]               Please try again                 []\n");
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
}

void incorrectInputlvl1() 
{
    printf("\n");
    printf("____________________________________________________\n")
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                 Your input:                    []\n");
    printf("[]                     %s                         []\n", characters_entered);
    printf("[]                  %s  =  %s                     []\n", alphaNumericList[tmpIndex], morseCodeMapping[tmpIndex]);
    printf("[]               Please try again                 []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    Lives--;//total lives decrements
    printf("\n");
    printf("____________________________________________________\n")
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]                      Wrong!                    []\n");
    printf("[]               You have lost a life.            []\n");
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    Wrong_Counter++;//wrong answers incremented
}


/* -------------------------------------------- Reading Inputs -------------------------------------------- */


// Initalize the global input array
void create_input_array()
{
    int maxsize = 20;
    for (int j = 0; j < maxsize; j++)
    {
        characters_entered[j] = NULL;//set all to null
    }
    input_complete = 0; //input complete = false
    i = 0; 
}

// Function to check input dot & dash
void read_input(int input)
{
    if (input == 1)
    {                           // 1 = "."
        characters_entered[i] = '.'; // add '.' to the array
        i++;
        printf(".");
    }
    else if (input == 2)
    {                           // 2 = "-"
        characters_entered[i] = '-'; // add '-' to the array
        i++;
        printf("-");
    }
    else if (input == 3)
    {                           // 3 = " "
        characters_entered[i] = ' '; // add ' ' to the array
        i++;
        printf(" ");
    }
    else if (input == 4 && input_state == 1){
        characters_entered[i-1] = NULL;  // 4 = "enter"

        input_complete = 1;  // value = 1 if answer is correct, otherwise remain 0
        select_level();
    }
    else if (input == 4 && input_state == 0){
        characters_entered[i-1] = NULL;  // 4 = "enter"

        input_complete = 1;  // value = 1 if answer is correct, otherwise remain 0
        display_input();
        RGB_Lives_Display(Lives); //update the LED colour accordingly
    }
}


/* -------------------------------------------- Checking Answer -------------------------------------------- */
int checkAnswer()
{
    if (strcmp(characters_entered,morseCodeMapping[tmpIndex])==0) //if user input = correct
    {
        return 1; //true
    }
    else
        return 0; 
}

int checkMorseCode()
{
    for (int i = 0; i < table_SIZE; i++) //Iterate the table
    {
        if (characters_entered == table[i].code) //Find corresponding morse char
        {
            return i;//return the morse char
        }
    }
    return -1; //should never reach this point
}


/* -------------------------------------------- Printing Game Messages -------------------------------------------- */
//When 5 questions are answered correctly in a level
void level_success()
{
    printf("\n");
    printf("========================================\n");
    printf("Complete current level %d!\n", Level_Index); //Print the current level number
    printf("========================================\n");
    printf("\n");
    //Stats from the level that was just completed
    printf("||========================================||\n");
    printf("||             PLAYER STATS               ||\n");
    printf("||             Correct times: 5           ||\n");
    printf("||             Wrong times: %d            ||\n", Wrong_Counter);
    printf("||             Gained Lives: %d           ||\n", Extra_Lives);
    printf("||             Remaining Lives: %d        ||\n", Lives);
    printf("||========================================||\n");
    printf("\n");
}

void game_won_display()
{
    printf("||=======================================||\n");
    printf("||                                       ||\n");
    printf("||      Congurtulations! You won!        ||\n");
    printf("||                                       ||\n");
    printf("||=======================================||\n");
}

void game_over_display()
{
    printf("||=================================||\n");
    printf("||                                 ||\n");
    printf("|| You lost! Better luck next time!||\n");
    printf("||                                 ||\n");
    printf("||=================================||\n");
}

void display_level_choosing()
{
    printf("\n");
    printf("Choose your difficulty level! Enter the number in morse code to select!\n");
    printf("LEVEL 1,: type the following: (.----)\n");
    printf("LEVEL 2,: type the following: (..---)\n");
    printf("LEVEL 3,: type the following: (...--)\n");
    printf("LEVEL 4,: type the following: (....-)\n");
}
//If none of the above were selected
void level_selection_invalid(){
    printf("||=======================================||\n");
    printf("||                                       ||\n");
    printf("|| You didn't choose a level! Try again! ||\n");
    printf("||                                       ||\n");
    printf("||=======================================||\n");
}

void level1_incorrect()
{
    printf("\n");
    printf("\n");
    printf("||================================================||\n");
    printf("||                You entered:                    ||\n");
    printf("                     %s                             \n", characters_entered);
    printf("                   %s  =  %s                        \n", morseCodeMapping[tmpIndex], morseCodeMappinghaNumericList[tmpIndex]);
    printf("||             This was NOT a match!              ||\n");
    printf("||================================================||\n");
    Lives--;//total lives decremented
    printf("\n");
    printf("||=================================||\n");
    printf("||      Wrong! You lost a life!    ||\n");
    printf("||=================================||\n");
    Wrong_Counter++;//increment wrong counter
}


void level1_correct()
{
    printf("\n");
    printf("\n");
    Number_Correct++;//correct answers incremened
    Remain_Counter--;//remaining questions decremented
    printf("||================================================||\n");
    printf("||                You entered:                    ||\n");
    printf("                   %s  =  %s                        \n", morseCodeMapping[tmpIndex], morseCodeMappinghaNumericList[tmpIndex]);
    printf("   You have correct %d time(s),  %d time(s) left    \n", Number_Correct, Remain_Counter);
    printf("||================================================||\n");
    if (Lives < 3)//only increment if lives have decreased
    {
        Lives++;//increment total lives counter
        printf("\n");
        printf("||=================================||\n");
        printf("|| Correct! You get an extra life! ||\n");
        printf("||=================================||\n");
        Extra_Lives++;//increment extra counter
    }
}

void display_input()
{
    if (tmpIndex >= 0 && tmpIndex <= 35) //index is in range
    {
        int check = checkAnswer();//evaluate answer
        if (input_complete == 1)  //if the user is finished with input
        {
            if (check == 0)
            {
                level1_incorrect(); //jump to incorrect conditions
            }
            else
            {
                level1_correct(); //jump to correct conditions
            }
        }
    }
    else
    {
        printf("Error detected");//throw error
    }
}



/* -------------------------------------------- Main function -------------------------------------------- */

int main()
{
    // Initialise all STDIO
    stdio_init_all();
    morse_init();
    watchdog_init();

    // Initialise the PIO interface with the WS2812 code (FOR RBG LED)
    // initialisePIO();
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, RGBW_STATE);
    put_pixel(urgb_u32(0x00, 0x00, 0x7F)); // Set the RGB LED color to blue

    //Print the welcome message 
    printf(INITIAL_MSG);

    // Jump into the main assembly code subroutine.
    watchdog_update();
    main_asm();

    // Returning zero indicates everything went okay.
    return 0;
}
